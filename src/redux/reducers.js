import { combineReducers } from 'redux';

import app from '@store/app';
import journey from '@store/journey';

export default combineReducers({
  app,
  journey,
});
