import Loadable from 'react-loadable';

import { Page as Loader } from '@components/Loader';

// eslint-disable-next-line import/prefer-default-export
export const LogoutComponent = Loadable({
  loader: () => import(/* webpackChunkName: "logout" */ '@routes/Logout/components/View'),
  loading: Loader,
});
