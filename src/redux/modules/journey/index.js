import journeyReducer from './reducer';

export { locationChange, backHistory } from './actions';

export default journeyReducer;
