import React, { PureComponent } from 'react';
import { shape, func, object } from 'prop-types';
import { connect } from 'react-redux';

import Container from '@components/Container';

class Page404 extends PureComponent {
  onlickBack = () => {
    const {
      previous: { pathname = '/', search = '' },
      history: { replace },
    } = this.props;

    replace({ pathname, search });
  };

  render() {
    return (
      <Container noSidebar noHeader noTitle>
        <div className="error">
          <div className="error__content">
            <h2>404</h2>
            <h3>Something went wrong!</h3>
            <p>There was a problem on our end. Please try again later.</p>
            <button type="button" className="btn btn-accent btn-pill" onClick={this.onlickBack}>
              &larr; Go Back
            </button>
          </div>
        </div>
      </Container>
    );
  }
}

Page404.propTypes = {
  history: shape({ replace: func.isRequired }).isRequired,
  previous: object,
};

Page404.defaultProps = {
  previous: { pathname: '/', search: '' },
};

const mapStateToProps = ({ journey }) => ({
  previous: journey.previous,
});

export default connect(mapStateToProps)(Page404);
