import Loadable from 'react-loadable';

import { Page as Loader } from '@components/Loader';

// eslint-disable-next-line import/prefer-default-export
export const LombaDetailComponent = Loadable({
  loader: () => import(/* webpackChunkName: "lomba-detail" */ '@routes/Lomba/Detail/components/View'),
  loading: Loader,
});
