import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { object } from 'prop-types';

import Page404 from '@components/404';
import { LombaTambahComponent } from './Tambah';
import { LombaDetailComponent } from './Detail';

function LombaRouter({ match }) {
  return (
    <Switch>
      <Route path={`${match.path}/tambah`} render={routeProps => <LombaTambahComponent {...routeProps} />} />
      <Route path={`${match.path}/detail/:id`} render={routeProps => <LombaDetailComponent {...routeProps} />} />
      <Route component={Page404} />
    </Switch>
  );
}

LombaRouter.propTypes = {
  match: object.isRequired,
};

export default LombaRouter;
