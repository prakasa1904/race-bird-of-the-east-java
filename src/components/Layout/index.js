import React, { Component } from 'react';
import { number, string, node, arrayOf, oneOfType } from 'prop-types';

import OnOffWrapper from '@components/OnOffWrapper';

class Layout extends Component {
  static propTypes = {
    children: oneOfType([node, string, number, arrayOf]).isRequired,
  };

  handleOnline = () => {
    document.body.classList.remove('grayscale');
  };

  handleOffline = () => {
    document.body.classList.add('grayscale');
  };

  render() {
    const { children } = this.props;

    return (
      <OnOffWrapper onOnline={this.handleOnline} onOffline={this.handleOffline}>
        {children}
      </OnOffWrapper>
    );
  }
}

export default Layout;
