import Loadable from 'react-loadable';

import { Page as Loader } from '@components/Loader';

// eslint-disable-next-line import/prefer-default-export
export const HomeComponent = Loadable({
  loader: () => import(/* webpackChunkName: "home" */ '@routes/Home/components/View'),
  loading: Loader,
});
