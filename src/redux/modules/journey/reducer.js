// import GA from '@lib/utils/GA';
import { removeTwoLastDuplicate } from '@helpers/journey';
import { LOCATION_CHANGE, BACK_HISTORY } from './actions';

function replace(visited, newVisit) {
  return visited.slice(0, visited.length - 1).concat([newVisit]);
}

const actionHandlers = {
  [LOCATION_CHANGE]: (state, payload) => {
    const actionPayload = payload.action;
    const isReplace = actionPayload === 'REPLACE';
    const visited = isReplace ? replace(state.visited, payload) : state.visited.concat([payload]);
    const newVisited = removeTwoLastDuplicate(visited);

    // log page view
    // const location = payload;

    setTimeout(() => {
      // GA.setPageView(location.pathname + location.search);
    }, 1500);

    return {
      ...state,
      visited: newVisited,
      previous: state.visited.pop(),
    };
  },
  [BACK_HISTORY]: (state, payload) => {
    const data = {
      ...state,
      visited: payload,
      previous: payload.pop(),
    };

    return data;
  },
};

const initialState = {
  visited: [],
  previous: '',
};

const journeyReducer = (state = initialState, { type, payload }) => {
  const hasAction = Object.prototype.hasOwnProperty.call(actionHandlers, type);

  return hasAction ? actionHandlers[type](state, payload) : state;
};

export default journeyReducer;
