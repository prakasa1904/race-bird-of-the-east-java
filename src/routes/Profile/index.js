import Loadable from 'react-loadable';

import { Page as Loader } from '@components/Loader';

// eslint-disable-next-line import/prefer-default-export
export const ProfileComponent = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ '@routes/Profile/components/View'),
  loading: Loader,
});
