import { makeActionCreator } from '@helpers/redux';
import { removeTwoLastDuplicate } from '@helpers/journey';

export const LOCATION_CHANGE = '@@journey/LOCATION_CHANGE';
export const locationChange = makeActionCreator(LOCATION_CHANGE);

export const BACK_HISTORY = '@@journey/BACK_HISTORY';
export function backHistory(currentJourney) {
  const newVisited = currentJourney.slice(0, currentJourney.length - 1);
  const lastVisited = newVisited[newVisited.length - 1];

  return dispatch => {
    dispatch({
      type: BACK_HISTORY,
      payload: removeTwoLastDuplicate(newVisited),
    });

    global.browserHistory.push({
      pathname: lastVisited.pathname,
      search: lastVisited.search,
      state: lastVisited.state,
    });
  };
}
