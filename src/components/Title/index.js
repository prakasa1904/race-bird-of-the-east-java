import React, { PureComponent } from 'react';
import { string } from 'prop-types';
import { connect } from 'react-redux';

import { ucFirst } from '@helpers/string';

class Title extends PureComponent {
  static propTypes = {
    title: string.isRequired,
  };

  render() {
    const { title } = this.props;

    return (
      <div className="page-header row no-gutters py-4">
        <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span className="text-uppercase page-subtitle">{ucFirst(title)}</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ app }) => ({
  title: app.title || '',
});

export default connect(mapStateToProps)(Title);
