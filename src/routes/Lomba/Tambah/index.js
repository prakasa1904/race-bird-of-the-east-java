import Loadable from 'react-loadable';

import { Page as Loader } from '@components/Loader';

// eslint-disable-next-line import/prefer-default-export
export const LombaTambahComponent = Loadable({
  loader: () => import(/* webpackChunkName: "lomba-tambah" */ '@routes/Lomba/Tambah/components/View'),
  loading: Loader,
});
