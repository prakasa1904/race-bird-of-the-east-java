// Don't prefer default export because we want helper functions to be tree-shake-able.
/* eslint-disable import/prefer-default-export */

/**
 * Get previous location from user's journey. Use this function instead of reading
 * `state.journey.previous` directly.
 * @param {visited} visited User's journey (which is array of visited location).
 * @returns {locationShape}
 */
export const getPrevious = visited => {
  if (visited.length < 2) {
    return {};
  }

  // "Previous" is second latest entry in `visited` array.
  return visited[visited.length - 2];
};

export const removeTwoLastDuplicate = visited => {
  const { length } = visited;

  if (length <= 1) {
    return visited;
  }

  const lastOne = visited[length - 1] || {};
  const lastTwo = visited[length - 2] || {};

  return lastOne.pathname.toString() === lastTwo.pathname.toString() && lastOne.search === lastTwo.search
    ? visited.slice(0, length - 1)
    : visited;
};
