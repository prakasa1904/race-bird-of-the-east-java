import React from 'react';
import { bool, object } from 'prop-types';

import Transparant from './Transparant';

const LoaderPage = ({ timedOut, error, pastDelay }) => {
  if (error || timedOut) {
    return null;
  }

  if (pastDelay) {
    return <Transparant />;
  }

  return null;
};

LoaderPage.defaultProps = {
  error: null,
};

LoaderPage.propTypes = {
  error: object,
  pastDelay: bool.isRequired,
  timedOut: bool.isRequired,
};

export default LoaderPage;
