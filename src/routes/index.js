import React from 'react';
import { object } from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import Layout from '@components/Layout';
import Page404 from '@components/404';

/**
 * List of routes
 */
import { HomeComponent } from '@routes/Home';
import { ProfileComponent } from '@routes/Profile';
import { LogoutComponent } from '@routes/Logout';
import LombaComponent from '@routes/Lomba';

const Routes = ({ history, location, match }) => (
  <Layout history={history} location={location} match={match}>
    <Switch>
      <Route exact path="/saya" component={ProfileComponent} />
      <Route path="/lomba" component={LombaComponent} />
      <Route exact path="/keluar" component={LogoutComponent} />
      <Route exact path="/" component={HomeComponent} />
      <Route component={Page404} />
    </Switch>
  </Layout>
);

Routes.propTypes = {
  history: object.isRequired,
  location: object.isRequired,
  match: object.isRequired,
};

const RootRoutes = () => <Route component={Routes} />;

export default RootRoutes;
