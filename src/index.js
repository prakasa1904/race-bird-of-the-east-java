import React from 'react';
import { hydrate, render } from 'react-dom';
import Loadable from 'react-loadable';
import { createBrowserHistory } from 'history';

import { locationChange } from '@store/journey';
import configureStore from './redux/configure';
import App from './App';

const history = createBrowserHistory();
const store = configureStore(window.__data);

global.terpusatBurung = store;
store.dispatch(locationChange({ ...history.location, action: history.action }));
history.listen((location, actionType) => {
  store.dispatch(locationChange({ ...location, action: actionType }));
});

Loadable.preloadReady().then(() => {
  const container = document.getElementById('devetek');
  const bootstrap = window.serverHTML ? hydrate : render;
  const props = {
    store,
    history,
  };

  bootstrap(<App {...props} />, container);
});
