import React from 'react';

import Container from '@components/Container';

class LombaDetailView extends React.Component {
  onClickFollow = () => {
    console.log('Following !!!');
  };

  render() {
    return (
      <Container>
        <div className="row mt-4">
          <div className="col-sm-12 col-lg-4">
            <div className="card card-small mb-4">
              <div className="card-body p-0">
                <div className="container-fluid">
                  <h4 className="text-center m-0 mt-2">Lomba Kemerdekaan</h4>
                  <p className="text-center text-light m-0 mb-2">Memperingati Hari Raya</p>
                  <div className="user-details__user-data border-top border-bottom p-4">
                    <div className="row mb-3">
                      <div className="col w-100">
                        <span>Lokasi</span>
                        <span>Mlangse Lor, Tubokarto, Wonogiri, Jawa Tengah</span>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col w-100">
                        <span>Penanggung Jawab</span>
                        <span>Prakasa Nedya Amrih</span>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col w-50">
                        <span>Kapasitas Peserta</span>
                        <span>1000</span>
                      </div>
                      <div className="col w-50">
                        <span>Peserta Terdaftar</span>
                        <span>10</span>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col w-100">
                        <span>Kontak</span>
                        <span>+6282 113 468 822</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-lg-8">
            <div className="card card-small user-teams mb-4">
              <div className="card-header border-bottom">
                <h6 className="m-0">Juara</h6>
                <div className="block-handle" />
              </div>
              <div className="card-body p-0">
                <div className="container-fluid">
                  <div className="row px-3">
                    <div className="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                      <img
                        alt="Nama Juara 1"
                        className="rounded"
                        src="https://designrevision.com/demo/shards-dashboards/images/user-profile/team-thumb-1.png"
                      />
                    </div>
                    <div className="col user-teams__info pl-3">
                      <h6 className="m-0">Team Edison</h6>
                      <span className="text-light">21 Members</span>
                    </div>
                  </div>
                  <div className="row px-3">
                    <div className="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                      <img
                        alt="Nama Juara 2"
                        className="rounded"
                        src="https://designrevision.com/demo/shards-dashboards/images/user-profile/team-thumb-2.png"
                      />
                    </div>
                    <div className="col user-teams__info pl-3">
                      <h6 className="m-0">Team Shelby</h6>
                      <span className="text-light">21 Members</span>
                    </div>
                  </div>
                  <div className="row px-3">
                    <div className="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                      <img
                        alt="Nama Juara 3"
                        className="rounded"
                        src="https://designrevision.com/demo/shards-dashboards/images/user-profile/team-thumb-3.png"
                      />
                    </div>
                    <div className="col user-teams__info pl-3">
                      <h6 className="m-0">Team Dante</h6>
                      <span className="text-light">21 Members</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    );
  }
}

export default LombaDetailView;
