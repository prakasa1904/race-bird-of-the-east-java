import React, { PureComponent } from 'react';
import { bool, number, string, node, arrayOf, oneOfType } from 'prop-types';

import Sidebar from '@components/Sidebar';
import Header from '@components/Header';
import Title from '@components/Title';

class Container extends PureComponent {
  static propTypes = {
    children: oneOfType([node, string, number, arrayOf]).isRequired,
    noHeader: bool,
    noSidebar: bool,
    noTitle: bool,
  };

  static defaultProps = {
    noHeader: false,
    noSidebar: false,
    noTitle: false,
  };

  render() {
    const { noSidebar, noHeader, noTitle, children } = this.props;

    return (
      <div className="h-100">
        {!noSidebar && <Sidebar />}
        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          {!noHeader && <Header />}
          <div className="main-content-container container-fluid px-4">
            {!noTitle && <Title />}
            {children}
          </div>
        </main>
      </div>
    );
  }
}

export default Container;
