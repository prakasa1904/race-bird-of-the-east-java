import React from 'react';

import Container from '@components/Container';

class LombaTambahView extends React.Component {
  onClickFollow = () => {
    console.log('Following !!!');
  };

  render() {
    return (
      <Container>
        <div className="row mt-4">
          <div className="col-sm-12 col-lg-12">
            <div className="card card-small edit-user-details mb-4">
              <div className="card-body p-0">
                <form className="py-4">
                  <div className="form-row mx-4">
                    <div className="col mb-3">
                      <h6 className="form-text m-0">Buat Lomba Burung</h6>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <div className="col-lg-8">
                      <div className="form-row">
                        <div className="form-group col-md-12">
                          <label htmlFor="content_name">Nama Lomba</label>
                          <input type="text" className="form-control" id="content_name" name="content_name" />
                        </div>
                        <div className="form-group col-md-12">
                          <label htmlFor="lastName">Last Name</label>
                          <input type="text" className="form-control" id="lastName" value="Brooks" />
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="userLocation">Location</label>
                          <div className="input-group input-group-seamless">
                            <div className="input-group-prepend">
                              <div className="input-group-text">
                                <i className="material-icons"></i>
                              </div>
                            </div>
                            <input type="text" className="form-control" value="Remote" />
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="phoneNumber">Phone Number</label>
                          <div className="input-group input-group-seamless">
                            <div className="input-group-prepend">
                              <div className="input-group-text">
                                <i className="material-icons"></i>
                              </div>
                            </div>
                            <input type="text" className="form-control" id="phoneNumber" value="+40 1234 567 890" />
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="emailAddress">Email</label>
                          <div className="input-group input-group-seamless">
                            <div className="input-group-prepend">
                              <div className="input-group-text">
                                <i className="material-icons"></i>
                              </div>
                            </div>
                            <input type="email" className="form-control" id="emailAddress" />
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label htmlFor="displayEmail">Display Email Publicly</label>
                          <select className="custom-select">
                            <option value="1" selected="">
                              Yes, display my email
                            </option>
                            <option value="2">No, do not display my email.</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <div className="form-group col-md-6">
                      <label htmlFor="userBio">Bio</label>
                      <textarea style={{ minHeight: '87px' }} id="userBio" name="userBio" className="form-control">
                        I&apos;m a design focused engineer.
                      </textarea>
                    </div>
                    <div className="form-group col-md-6">
                      <label htmlFor="userTags">Tags</label>
                      <div className="bootstrap-tagsinput">
                        <span className="tag label label-info">
                          User Experience
                          <span data-role="remove" />
                        </span>{' '}
                        <span className="tag label label-info">
                          UI Design
                          <span data-role="remove" />
                        </span>{' '}
                        <span className="tag label label-info">
                          {' '}
                          React JS
                          <span data-role="remove" />
                        </span>{' '}
                        <span className="tag label label-info">
                          {' '}
                          HTML &amp; CSS
                          <span data-role="remove" />
                        </span>{' '}
                        <span className="tag label label-info">
                          {' '}
                          JavaScript
                          <span data-role="remove" />
                        </span>{' '}
                        <span className="tag label label-info">
                          {' '}
                          Bootstrap 4<span data-role="remove" />
                        </span>{' '}
                        <input type="text" placeholder="" />
                      </div>
                      <input
                        id="userTags"
                        name="userTags"
                        value="User Experience,UI Design, React JS, HTML &amp; CSS, JavaScript, Bootstrap 4"
                        className="d-none"
                      />
                    </div>
                  </div>
                  <hr />
                  <div className="form-row mx-4">
                    <div className="col mb-3">
                      <h6 className="form-text m-0">Social</h6>
                      <p className="form-text text-muted m-0">Setup your social profiles info.</p>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <div className="form-group col-md-4">
                      <label htmlFor="socialFacebook">Facebook</label>
                      <div className="input-group input-group-seamless">
                        <div className="input-group-prepend">
                          <div className="input-group-text">
                            <i className="fab fa-facebook-f" />
                          </div>
                        </div>
                        <input type="text" className="form-control" id="socialFacebook" />
                      </div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="socialTwitter">Twitter</label>
                      <div className="input-group input-group-seamless">
                        <div className="input-group-prepend">
                          <div className="input-group-text">
                            <i className="fab fa-twitter" />
                          </div>
                        </div>
                        <input type="email" className="form-control" id="socialTwitter" />
                      </div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="socialGitHub">GitHub</label>
                      <div className="input-group input-group-seamless">
                        <div className="input-group-prepend">
                          <div className="input-group-text">
                            <i className="fab fa-github" />
                          </div>
                        </div>
                        <input type="text" className="form-control" id="socialGitHub" />
                      </div>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <div className="form-group col-md-4">
                      <label htmlFor="socialSlack">Slack</label>
                      <div className="input-group input-group-seamless">
                        <div className="input-group-prepend">
                          <div className="input-group-text">
                            <i className="fab fa-slack" />
                          </div>
                        </div>
                        <input type="email" className="form-control" id="socialSlack" />
                      </div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="socialDribbble">Dribbble</label>
                      <div className="input-group input-group-seamless">
                        <div className="input-group-prepend">
                          <div className="input-group-text">
                            <i className="fab fa-dribbble" />
                          </div>
                        </div>
                        <input type="email" className="form-control" id="socialDribbble" />
                      </div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="socialGoogle">Google Plus+</label>
                      <div className="input-group input-group-seamless">
                        <div className="input-group-prepend">
                          <div className="input-group-text">
                            <i className="fab fa-google-plus-g" />
                          </div>
                        </div>
                        <input type="email" className="form-control" id="socialGoogle" />
                      </div>
                    </div>
                  </div>
                  <hr />
                  <div className="form-row mx-4">
                    <div className="col mb-3">
                      <h6 className="form-text m-0">Notifications</h6>
                      <p className="form-text text-muted m-0">Setup which notifications would you like to receive.</p>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <label htmlFor="conversationsEmailsToggle" className="col col-form-label">
                      {' '}
                      Conversations{' '}
                      <small className="form-text text-muted">
                        {' '}
                        Sends notification emails with updates for the conversations you are participating in or if
                        someone mentions you.{' '}
                      </small>
                    </label>
                    <div className="col d-flex">
                      <div className="custom-control custom-toggle ml-auto my-auto">
                        <input
                          type="checkbox"
                          id="conversationsEmailsToggle"
                          className="custom-control-input"
                          checked=""
                        />
                        <label className="custom-control-label" htmlFor="conversationsEmailsToggle" />
                      </div>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <label htmlFor="newProjectsEmailsToggle" className="col col-form-label">
                      {' '}
                      New Projects{' '}
                      <small className="form-text text-muted">
                        {' '}
                        Sends notification emails when you are invited to a new project.{' '}
                      </small>
                    </label>
                    <div className="col d-flex">
                      <div className="custom-control custom-toggle ml-auto my-auto">
                        <input type="checkbox" id="newProjectsEmailsToggle" className="custom-control-input" />
                        <label className="custom-control-label" htmlFor="newProjectsEmailsToggle" />
                      </div>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <label htmlFor="vulnerabilitiesEmailsToggle" className="col col-form-label">
                      {' '}
                      Vulnerability Alerts{' '}
                      <small className="form-text text-muted">
                        {' '}
                        Sends notification emails when everything goes down and there&apos;s no hope left whatsoever.{' '}
                      </small>
                    </label>
                    <div className="col d-flex">
                      <div className="custom-control custom-toggle ml-auto my-auto">
                        <input
                          type="checkbox"
                          id="vulnerabilitiesEmailsToggle"
                          className="custom-control-input"
                          checked=""
                        />
                        <label className="custom-control-label" htmlFor="vulnerabilitiesEmailsToggle" />
                      </div>
                    </div>
                  </div>
                  <hr />
                  <div className="form-row mx-4">
                    <div className="col mb-3">
                      <h6 className="form-text m-0">Change Password</h6>
                      <p className="form-text text-muted m-0">Change your current password.</p>
                    </div>
                  </div>
                  <div className="form-row mx-4">
                    <div className="form-group col-md-4">
                      <label htmlFor="firstName">Old Password</label>
                      <input type="text" className="form-control" id="firstName" placeholder="Old Password" />
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="lastName">New Password</label>
                      <input type="text" className="form-control" id="lastName" placeholder="New Password" />
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="emailAddress">Repeat New Password</label>
                      <input
                        type="email"
                        className="form-control"
                        id="emailAddress"
                        placeholder="Repeat New Password"
                      />
                    </div>
                  </div>
                </form>
              </div>
              <div className="card-footer border-top">
                <a href="/" className="btn btn-sm btn-accent ml-auto d-table mr-3">
                  Save Changes
                </a>
              </div>
            </div>
          </div>
        </div>
      </Container>
    );
  }
}

export default LombaTambahView;
